FROM node
WORKDIR /src
COPY . .
EXPOSE 8090
CMD node server.js
